package cn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@SpringBootApplication
@EnableScheduling
public class WebServiceClientApplication {


    public static void main(String[] args) {
        SpringApplication.run(WebServiceClientApplication.class, args);

    }

    @Scheduled(fixedDelay = 5000)
    public void text() {
        System.out.println("111");
    }
}
