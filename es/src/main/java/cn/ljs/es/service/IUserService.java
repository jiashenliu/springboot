package cn.ljs.es.service;

import cn.ljs.es.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author author
 * @since 2024-01-20
 */
public interface IUserService extends IService<User> {

}
