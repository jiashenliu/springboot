package cn.ljs.es.service.impl;

import cn.ljs.es.entity.User;
import cn.ljs.es.mapper.UserMapper;
import cn.ljs.es.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author author
 * @since 2024-01-20
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}
