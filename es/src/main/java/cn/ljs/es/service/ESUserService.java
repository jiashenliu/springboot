package cn.ljs.es.service;

import cn.ljs.es.dao.ESUserDao;
import cn.ljs.es.entity.ESUser;
import cn.ljs.es.entity.User;
import cn.ljs.es.mapper.UserMapper;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ESUserService {

    @Autowired
    private ESUserDao esUserDao;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;


    public Iterable<ESUser> instESUserList() {
        List<User> list = userMapper.selectList(null);
        List<ESUser> esUsers = new ArrayList<>();
        for (User user : list) {
            ESUser esUser = new ESUser();
            BeanUtils.copyProperties(user, esUser);
            esUsers.add(esUser);
        }
        return esUserDao.saveAll(esUsers);
    }


    public List<User> findESUserlist() {
        List<User> users = new ArrayList<>();
        Iterable<ESUser> all = esUserDao.findAll();
        for (ESUser esUser : all) {
            User user = new User();
            BeanUtils.copyProperties(esUser, user);
            users.add(user);
        }
        return users;
    }

    public List<ESUser> EsUserByName(String name, Integer id) {
        NativeSearchQuery query = new NativeSearchQueryBuilder()
                .withQuery(new BoolQueryBuilder()
                        .must(QueryBuilders.wildcardQuery("name", name + "?"))
                        .must(QueryBuilders.matchQuery("id", id)))
                .build();

        SearchHits<ESUser> search = elasticsearchRestTemplate.search(query, ESUser.class);
        List<ESUser> users = new ArrayList<>();
        for (SearchHit<ESUser> esUserSearchHit : search) {
            users.add(esUserSearchHit.getContent());
        }
        return users;
    }
}
