package cn.ljs.es.controller;


import cn.ljs.es.entity.ESUser;
import cn.ljs.es.entity.User;
import cn.ljs.es.service.ESUserService;
import cn.ljs.es.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author author
 * @since 2024-01-20
 */
@RestController
public class ESUserController {

    @Autowired
    private IUserService userService;

    @Autowired
    private ESUserService esUserService;

    @GetMapping("/users")
    public List<User> getUserlist() {
        return userService.list();
    }

    @PostMapping("/save_es_user")
    public Iterable<ESUser> saveESUser() {
        return esUserService.instESUserList();
    }

    @GetMapping("/es_users")
    public List<User> getESUserlist() {
        return esUserService.findESUserlist();
    }

//    @GetMapping("/es_users")
//    public List<ESUser> getESUserByName(@RequestParam String name, @RequestParam Integer id) {
//        return esUserService.EsUserByName(name,id);
//    }
}
