package cn.ljs.es.dao;

import cn.ljs.es.entity.ESUser;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ESUserDao extends ElasticsearchRepository<ESUser, Integer> {

}
