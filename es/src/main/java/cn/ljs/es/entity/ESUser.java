package cn.ljs.es.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.util.Date;

@Data
@Document(indexName = "user")
public class ESUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Integer id;

    @Field(type = FieldType.Text, analyzer = "ik_smart")
    private String name;

    private Integer state;

    private Integer version;

    private String password;


    private Date addTime;

    public ESUser(Integer id, String name, Integer state, Integer version, String password) {
        this.id = id;
        this.name = name;
        this.state = state;
        this.version = version;
        this.password = password;
    }

    public ESUser() {
    }
}
