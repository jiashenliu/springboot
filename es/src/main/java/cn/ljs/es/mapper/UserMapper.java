package cn.ljs.es.mapper;

import cn.ljs.es.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author author
 * @since 2024-01-20
 */
public interface UserMapper extends BaseMapper<User> {

}
