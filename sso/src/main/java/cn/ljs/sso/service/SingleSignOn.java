package cn.ljs.sso.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class SingleSignOn {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    // 用户登录，生成token并存储到Redis
    public String loginUser(String userName) {
        String token = generateToken(userName);
        storeTokenInRedis(token, userName);
        return token;
    }

    // 使用token进行身份验证
    public String getUserInfo(String token) {
        return redisTemplate.opsForValue().get(token);
    }

    // 用户注销，从Redis中删除token信息
    public void logoutUser(String token) {
        redisTemplate.delete(token);
    }


    private String generateToken(String userName) {
        // 实际业务中可以使用UUID等方式生成唯一token 传到前端保存在浏览器的localStorage或者cookie
        return "generated_token_for_" + userName;
    }

    private void storeTokenInRedis(String token, String userName) {
        ValueOperations<String, String> ops = redisTemplate.opsForValue();
        ops.set(token, userName, 60, TimeUnit.SECONDS);
    }
}