package cn.ljs.sso.controller;

import cn.ljs.sso.service.SingleSignOn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SsoCtl {

    @Autowired
    private SingleSignOn singleSignOn;


    @RequestMapping("/login")
    public String loginUser(@RequestParam String userName) {
        return singleSignOn.loginUser(userName);
    }

    @RequestMapping("/get_user")
    public String getUserInfo(@RequestHeader String token) {
        return singleSignOn.getUserInfo(token);
    }

    @RequestMapping("/out")
    public void logoutUser(@RequestHeader String token) {
        System.out.println(token);
        singleSignOn.logoutUser(token);
    }

}
