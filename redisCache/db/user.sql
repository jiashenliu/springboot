create table users
(
    id       int auto_increment
        primary key,
    name     varchar(255) null,
    state    int          null,
    version  int          null,
    password varchar(255) null
);
