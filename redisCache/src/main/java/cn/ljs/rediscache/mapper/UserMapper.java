package cn.ljs.rediscache.mapper;

import cn.ljs.rediscache.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author author
 * @since 2024-01-16
 */
public interface UserMapper extends BaseMapper<User> {

}
