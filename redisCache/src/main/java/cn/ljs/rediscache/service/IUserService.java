package cn.ljs.rediscache.service;

import cn.ljs.rediscache.entity.User;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author author
 * @since 2024-01-16
 */
public interface IUserService extends IService<User> {
    public User getUser(Integer id);

    public User get(Integer id);
}
