package cn.ljs.rediscache.service.impl;

import cn.ljs.rediscache.entity.User;
import cn.ljs.rediscache.mapper.UserMapper;
import cn.ljs.rediscache.service.IUserService;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author author
 * @since 2024-01-16
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    private final String key = "redisCache::redis_user_";

    @Autowired
    private RedisTemplate<String, String> redisTemplate;


    @Transactional
    @Cacheable(value = "redisCache", key = "'redis_user_'+#id")
    public User getUser(Integer id) {
        return getById(id);
    }

    @Transactional
    @CachePut(value = "redisCache", key = "'redis_user_'+#id")
    public User get(Integer id) {
        String keys = key + id;
        User user = JSON.parseObject(redisTemplate.opsForValue().get(keys), User.class);
        return user != null ? user : getById(id);
    }
}
