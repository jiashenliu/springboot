package cn.ljs.rediscache.controller;


import cn.ljs.rediscache.entity.User;
import cn.ljs.rediscache.service.IUserService;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author author
 * @since 2024-01-16
 */
@RestController
@Slf4j
public class UserController {

    @Autowired
    private IUserService userService;

    @RequestMapping("/get_by_id/{id}")
    @ResponseBody
    public User getUser(@PathVariable Integer id) {
        return userService.getUser(id);
    }

    @RequestMapping("/get/{id}")
    @ResponseBody
    public User get(@PathVariable Integer id) {
        return (User) userService.get(id);
    }
}
