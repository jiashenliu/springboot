package cn.ljs.webservice;

import cn.ljs.webservice.entity.User;
import cn.ljs.webservice.entity.UserDTO;
import cn.ljs.webservice.service.UserDTOService;
import cn.ljs.webservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@Service
@WebService
public class MainServer {

    @Autowired
    private UserDTOService userDTOService;

    @Autowired
    private UserService userService;


    public UserDTO getUser(Long id) {
        return userDTOService.getUser(id);
    }

    public User get(Long id) {
        return userService.get(id);
    }

    public List<User> getList(Long id) {
        return userService.getList();
    }

}
