package cn.ljs.webservice.service;

import cn.ljs.webservice.entity.UserDTO;

public interface UserDTOService {
    public UserDTO getUser(Long id);
}
