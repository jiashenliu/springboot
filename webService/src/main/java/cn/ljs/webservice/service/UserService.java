package cn.ljs.webservice.service;

import cn.ljs.webservice.entity.User;

import java.util.ArrayList;
import java.util.List;

public interface UserService {

    public User get(Long id);

    public List<User> getList();
}
