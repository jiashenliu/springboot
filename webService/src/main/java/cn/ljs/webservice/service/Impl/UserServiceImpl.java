package cn.ljs.webservice.service.Impl;

import cn.ljs.webservice.entity.User;
import cn.ljs.webservice.entity.UserDTO;
import cn.ljs.webservice.service.UserService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {


    public User get(Long id) {
        User user = new User();
        user.setId(id);
        user.setName("gongj");
        System.out.println(user);
        return user;
    }

    public List<User> getList() {
        List<User> users = new ArrayList<>();
        User user = new User();
        user.setId(1L);
        user.setName("gongj");
        User user1 = new User();
        user1.setId(2L);
        user1.setName("gongj1");
        users.add(user);
        users.add(user1);
        System.out.println(users);
        return users;
    }
}
