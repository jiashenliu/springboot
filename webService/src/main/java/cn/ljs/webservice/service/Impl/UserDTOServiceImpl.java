package cn.ljs.webservice.service.Impl;

import cn.ljs.webservice.entity.UserDTO;
import cn.ljs.webservice.service.UserDTOService;
import org.springframework.stereotype.Service;

@Service
public class UserDTOServiceImpl implements UserDTOService {


    public UserDTO getUser(Long id) {
        UserDTO user = new UserDTO();
        user.setId(id);
        user.setAddress("上海市浦东新区");
        user.setAge(25);
        user.setName("gongj");
        System.out.println(user);
        return user;
    }
}
