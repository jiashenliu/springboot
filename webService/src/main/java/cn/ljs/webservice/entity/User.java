package cn.ljs.webservice.entity;

import lombok.Data;

@Data
public class User {
    private Long id;
    private String name;
}
