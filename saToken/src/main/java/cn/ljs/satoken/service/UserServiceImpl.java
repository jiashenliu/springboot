package cn.ljs.satoken.service;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ljs.satoken.entity.User;
import cn.ljs.satoken.mapper.UserMapper;
import cn.ljs.satoken.service.impl.UserService;
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService{

}
