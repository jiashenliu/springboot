package cn.ljs.reids.controller;


import cn.ljs.reids.server.SmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PhoneCheckCtl {

  @Autowired
  private SmsService smsService;

    @RequestMapping("/send")
    public void sendSms(){
        smsService.sendSms("17630593078", "123456");
    }

    @RequestMapping("/check")
    public void checkSms(){
        smsService.checkSms("17630593078", "123456");
    }
}
