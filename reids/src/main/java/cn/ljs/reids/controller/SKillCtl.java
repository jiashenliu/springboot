package cn.ljs.reids.controller;

import cn.ljs.reids.server.PlayerScoreService;
import cn.ljs.reids.server.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SKillCtl {

    @Autowired
    private SkillService skillService;

    @RequestMapping("/add/{pId}/{num}")
    public void addProduct(@PathVariable String pId,@PathVariable Integer num){
        skillService.addProduct(pId,num);
    }

    @RequestMapping("/kill/{pId}/{num}")
    public void kill(@PathVariable String pId,@PathVariable Integer num) {
        boolean b = skillService.skillProduct(pId, num);

        if (b) {
            System.out.println("成功");
        }else{
            System.out.println("失败");
        }
    }
}
