package cn.ljs.reids.controller;

import cn.ljs.reids.server.PlayerScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
public class PlayerSourceCtl {
    @Autowired
    private PlayerScoreService playerScoreService;

    @RequestMapping("/add_source/{name}/{score}")
    public void addSource(@PathVariable String name,@PathVariable Double score) {
        playerScoreService.addPlayerScore(name, score);
    }

    @RequestMapping("/get_by_name/{name}")
    @ResponseBody
    public Double getSourceByName(@PathVariable String name) {
        return playerScoreService.getPlayerScore(name);
    }

    @RequestMapping("/get_top/{top}")
    @ResponseBody
    public Set<String> getSourceTop(@PathVariable Integer top) {
        return playerScoreService.getTopPlayers(top);
    }
    
}
