package cn.ljs.reids.controller;

import cn.ljs.reids.server.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class CardCtl {

    @Autowired
    private CardService cardService;

    @RequestMapping("/add")
    public void addCard(@RequestParam String userId, @RequestParam String productId,@RequestParam Integer num){
        cardService.addToCart(userId,productId,num);
    }

    @RequestMapping("/del")
    public void delCard(@RequestParam String userId, @RequestParam String productId){
        cardService.removeFromCart(userId,productId);
    }

    @RequestMapping("/list")
    public Map<Object, Object> listCard(@RequestParam String userId){
         return cardService.getCartItems(userId);
    }

}
