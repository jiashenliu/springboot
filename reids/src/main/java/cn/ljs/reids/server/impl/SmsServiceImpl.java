package cn.ljs.reids.server.impl;

import cn.ljs.reids.server.SmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisStringCommands;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.types.Expiration;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class SmsServiceImpl implements SmsService {


    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    public void sendSms(String phone, String code) {
        // 使用setnx命令设置验证码
        if (setNx(phone, code, 10, TimeUnit.DAYS)) {
            // 验证码设置成功
        } else {
            // 验证码已存在
        }
    }

    public void checkSms(String phone, String code) {
        // 从Redis中获取验证码
        String redisCode = redisTemplate.opsForValue().get(phone);

        // 验证码不存在或已过期
        if (redisCode == null || redisCode.isEmpty()) {
            return;
        }

        // 验证码不匹配
        if (!redisCode.equals(code)) {
            return;
        }

        System.out.println("验证码匹配，删除验证码");
        redisTemplate.delete(phone);

    }

    /*
    不存在就添加
   */
    public boolean setNx(String key, String value, long expires, TimeUnit timeUnit)
    {
        boolean flag = false;
        try {
            flag = (boolean) redisTemplate.execute((RedisCallback<Boolean>) connection -> connection.set(key.getBytes(), value.getBytes(), Expiration.from(expires, timeUnit), RedisStringCommands.SetOption.ifAbsent()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }
}