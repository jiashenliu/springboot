package cn.ljs.reids.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.Set;


public interface PlayerScoreService {

    public void addPlayerScore(String name, double score);

    public Double getPlayerScore(String name);

    public Set<String> getTopPlayers(int topN);
}
