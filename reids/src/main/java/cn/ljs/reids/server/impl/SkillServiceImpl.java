package cn.ljs.reids.server.impl;

import cn.ljs.reids.server.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class SkillServiceImpl implements SkillService {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    public boolean skillProduct(String productId, int quantity) {
        String lockKey = "skill_lock:" + productId;
        Boolean locked = redisTemplate.opsForValue().setIfAbsent(lockKey, "locked", 30, TimeUnit.SECONDS);

        if (locked != null && locked) {
            try {

                String s = redisTemplate.opsForValue().get("stock:" + productId);
                int stock= s!=null ? Integer.parseInt(s) : 0;

                if (stock >= quantity) {
                    redisTemplate.opsForValue().decrement("stock:" + productId, quantity);
                    return true; // 成功
                } else {
                    return false; // 库存不足，失败
                }
            } finally {
                redisTemplate.delete(lockKey); // 释放锁
            }
        } else {
            return false; // 获取锁失败
        }
    }

    @Override
    public void addProduct(String productId, Integer num) {
        redisTemplate.opsForValue().set("stock:" + productId,num.toString());
    }
}