package cn.ljs.reids.server;

public interface SkillService {

    boolean skillProduct(String productId, int quantity);

    void addProduct(String productId,Integer num);
}
