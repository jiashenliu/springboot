package cn.ljs.reids.server;

import java.util.concurrent.TimeUnit;

public interface SmsService {

    public void sendSms(String phone, String code);

    public void checkSms(String phone, String code);

    public boolean setNx(String key, String value, long expires, TimeUnit timeUnit);
}
