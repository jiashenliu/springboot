package cn.ljs.reids.server;


import java.util.Map;

public interface CardService {



    public void addToCart(String userId, String productId, Integer num);

    public void removeFromCart(String userId, String productId);

    public Map<Object, Object> getCartItems(String userId);
}
