package cn.ljs.reids.server.impl;

import cn.ljs.reids.server.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Service
public class CardServiceImpl implements CardService {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    RestTemplate restTemplate;


    @Override
    public void addToCart(String userId, String productId, Integer num) {
        redisTemplate.opsForHash().increment("cart:" +userId,productId,num);


    }

    @Override
    public void removeFromCart(String userId, String productId) {
    redisTemplate.opsForHash().delete("cart:" +userId,productId);
    }

    @Override
    public Map<Object, Object> getCartItems(String userId) {
        return redisTemplate.opsForHash().entries("cart:" + userId);
    }
}
