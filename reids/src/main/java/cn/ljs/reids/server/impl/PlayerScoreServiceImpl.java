package cn.ljs.reids.server.impl;

import cn.ljs.reids.server.PlayerScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class PlayerScoreServiceImpl implements PlayerScoreService {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;



    public void addPlayerScore(String name, double score) {
        redisTemplate.opsForZSet().add("leaderboard",name,score);
    }

    public Double getPlayerScore(String name) {
        return redisTemplate.opsForZSet().score("leaderboard", name);
    }

    public Set<String> getTopPlayers(int topN) {
        return redisTemplate.opsForZSet().reverseRange("leaderboard", 0, topN - 1);
    }
}
